var app = angular.module('app', [
    'ngRoute',
    'ngResource',
    'ngAnimate',
    'ui.bootstrap'
]);

app.run(['$rootScope', '$http',
    function ($rootScope, $http) {
        $rootScope.appUrl = 'http://localhost:8080/its-userbook';
    }]);

app.config([
    '$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/', {templateUrl: 'static/view/home.html'})
            .when('/users/:userId', {templateUrl: 'static/view/user.html'})
            .when('/users', {templateUrl: 'static/view/users.html'})
            .when('/about', {templateUrl: 'static/view/about.html'})
            .otherwise({
                redirectTo: '/'
            });

    }
]);