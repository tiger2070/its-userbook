app.controller('UserController', [
    '$scope', '$rootScope', '$http', '$routeParams',
    function ($scope, $rootScope, $http, $routeParams) {
        var userId = $routeParams.userId;

        $http.get($rootScope.appUrl + "/api/users/" + userId)
            .then(function (response) {
                $scope.user = response.data.data;
            }, function (response) {
                console.log("error while loading user #" + userId);
            });


    }
]);