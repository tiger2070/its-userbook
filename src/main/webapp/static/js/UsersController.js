app.controller('UsersController', [
    '$scope', '$rootScope', '$http', '$routeParams', '$window',
    function ($scope, $rootScope, $http, $routeParams, $window) {

        var page = $routeParams.page != null ? $routeParams.page : 1;
        var itemsPerPage = $routeParams.itemsPerPage;
        $scope.itemsPerPage = itemsPerPage != null ? itemsPerPage : 10;
        $scope.sort = $routeParams.sort != null ? $routeParams.sort : 'id';

        $http.get($rootScope.appUrl + "/api/users?pageNum=" + page + "&limit=" + $scope.itemsPerPage + "&sort=" + $scope.sort)
            .then(function (response) {
                $scope.currentPage = page;
                $scope.totalItems = response.data.data.totalElements;
                $scope.users = response.data.data.users;
            }, function (response) {
                console.log("error while loading users");
            });

        $scope.delete = function (id, index) {
            $http.delete($rootScope.appUrl + "/api/users/" + id)
                .then(function (response) {
                    $scope.users.splice(index, 1);
                }, function (response) {
                    console.log("error while deleting user #:" + id);
                });
        };

        $scope.pageChanged = function () {
            $window.location.href = "#/users?page=" + $scope.currentPage + "&itemsPerPage=" +
                $scope.itemsPerPage + "&sort=" + $scope.sort;
        };

    }
]);