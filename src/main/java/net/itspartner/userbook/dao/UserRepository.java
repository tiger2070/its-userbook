package net.itspartner.userbook.dao;

import net.itspartner.userbook.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User,Integer> {

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.hobbies WHERE u.id = (:id)")
    User getUserWithHobbies(@Param("id") Integer id);
}
