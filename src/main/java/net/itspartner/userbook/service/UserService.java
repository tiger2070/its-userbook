package net.itspartner.userbook.service;

import net.itspartner.userbook.model.User;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {
    void create(User user);

    void delete(Integer id);

    Page<User> getUsers(int pageNum, int limit, String sort);

    User getUserWithHobbies(Integer id);

}
