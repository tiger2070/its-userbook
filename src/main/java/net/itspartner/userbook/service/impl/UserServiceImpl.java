package net.itspartner.userbook.service.impl;

import net.itspartner.userbook.api.exception.IncorrectUserIdException;
import net.itspartner.userbook.dao.UserRepository;
import net.itspartner.userbook.model.User;
import net.itspartner.userbook.model.enumeration.Sorting;
import net.itspartner.userbook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private static final int USERS_HARD_LIMIT = 100;
    private static final String DEFAULT_SORTING_FIELD = "id";
    @Autowired
    private UserRepository userRepository;

    @Override
    public void create(User user) {
        userRepository.save(user);
    }

    @Override
    public void delete(Integer id) {
        User found = userRepository.findOne(id);
        if (found == null) {
            throw new IncorrectUserIdException("no such user with id: " + id);
        }
        userRepository.delete(id);
    }

    @Override
    public Page<User> getUsers(int pageNum, int limit, String sort) {
        int pageNumParam = 0;
        if (pageNum > 0) {
            pageNumParam = pageNum - 1;
        }
        if (limit > USERS_HARD_LIMIT) {
            limit = USERS_HARD_LIMIT;
        }
        String sortingField = validateSortingField(sort);
        return userRepository.findAll(new PageRequest(pageNumParam, limit, Sort.Direction.ASC, sortingField));
    }

    @Override
    public User getUserWithHobbies(Integer id) {
        return userRepository.getUserWithHobbies(id);
    }

    private String validateSortingField(String sort) {
        if (Sorting.getStringValues().contains(sort)) {
            return sort;
        } else {
            return DEFAULT_SORTING_FIELD;
        }
    }
}
