package net.itspartner.userbook.model.enumeration;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public enum Sorting {
    ID("id"), AGE("age"), GENDER("gender"), LASTNAME("lastName"), FIRSTNAME("firstName"), CREATIONTIME("creationTime");

    private String fieldName;

    public String getFieldName() {
        return fieldName;
    }

    Sorting(String fieldName) {
        this.fieldName = fieldName;
    }

    public static Set<String> getStringValues() {
        Set<String> result = Arrays.asList(Sorting.values()).stream()
                .map(Sorting::getFieldName)
                .collect(Collectors.toSet());
        return result;
    }
}
