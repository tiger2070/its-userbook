package net.itspartner.userbook.model.enumeration;

public enum  Gender {
    MALE, FEMALE
}
