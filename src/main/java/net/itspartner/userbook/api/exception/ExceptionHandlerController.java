package net.itspartner.userbook.api.exception;

import net.itspartner.userbook.api.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlerController {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Response othersException(Exception e) {
        return Response.createErrorResponse(e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IncorrectUserIdException.class)
    @ResponseBody
    public Response incorrectUserIdException(IncorrectUserIdException e) {
        return Response.createErrorResponse(e.getMessage());
    }
}
