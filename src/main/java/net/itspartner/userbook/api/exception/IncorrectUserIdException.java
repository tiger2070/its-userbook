package net.itspartner.userbook.api.exception;

public class IncorrectUserIdException extends RuntimeException {
    public IncorrectUserIdException() {
    }

    public IncorrectUserIdException(String message) {
        super(message);
    }

    public IncorrectUserIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectUserIdException(Throwable cause) {
        super(cause);
    }

    public IncorrectUserIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
