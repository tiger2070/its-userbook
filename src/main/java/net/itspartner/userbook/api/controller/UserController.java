package net.itspartner.userbook.api.controller;

import net.itspartner.userbook.api.response.Response;
import net.itspartner.userbook.api.response.UserDto;
import net.itspartner.userbook.api.response.UsersResult;
import net.itspartner.userbook.model.User;
import net.itspartner.userbook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public Response getUsers(@RequestParam("pageNum") int pageNum, @RequestParam("limit") int limit,
                             @RequestParam("sort") String sort) {
        Page<User> page = userService.getUsers(pageNum, limit, sort);
        long totalElements = page.getTotalElements();
        List<UserDto> userDtos = page.getContent().stream()
                .map(user -> user.convertToUserDto(false))
                .collect(Collectors.toList());
        return Response.createSuccessResponse(new UsersResult(totalElements, userDtos));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Response getUser(@PathVariable("id") Integer id) {
        return Response.createSuccessResponse(userService.getUserWithHobbies(id).convertToUserDto(true));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Response deleteUser(@PathVariable("id") Integer id) {
        userService.delete(id);
        return Response.createSuccessResponse("OK");
    }


}
