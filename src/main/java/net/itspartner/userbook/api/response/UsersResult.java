package net.itspartner.userbook.api.response;

import net.itspartner.userbook.model.User;

import java.util.List;

public class UsersResult {
    private long totalElements;
    private List<UserDto> users;

    public UsersResult(long totalElements, List<UserDto> users) {
        this.totalElements = totalElements;
        this.users = users;
    }

    public UsersResult() {
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }
}
