INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Lionel', 'Messi', 34, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Cristiano', 'Ronaldo', 31, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Andres', 'Iniesta', 25, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Zlatan', 'Ibrahimovic', 45, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Radamel', 'Falcao', 27, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Robin', 'van Persie', 30, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Andrea', 'Pirlo', 32, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Yaya', 'Toure', 37, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Edinson', 'Cavani', 27, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Sergio', 'Aguero', 29, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Iker', 'Casillas', 29, 'MALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Mana', 'Alison', 26, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Linda', 'Caruso', 33, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Carole', 'Duffy', 35, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Sami', 'Grisafe', 45, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Jenny', 'Schmidt', 25, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Erin', 'Sheriff', 25, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Josie', 'Smith-Malave', 36, 'FEMALE', NOW());
INSERT INTO USER (FIRST_NAME, LAST_NAME, AGE, GENDER, CREATION_TIME) VALUES ('Whitney', 'Zelee', 39, 'FEMALE', NOW());