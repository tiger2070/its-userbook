﻿# To run "userbook" test-application you need execute next steps: #
1. CREATE DATABASE userbook CHARACTER SET utf8 COLLATE utf8_general_ci. Creates MySQL database(use 5.6+ version).
2.  a) src -> main -> resources -> app.properties. Set your db user and password.
    b) build.gradle -> section flyway (70 line). Set your db user and password.
3.  gradle flywayMigrate. Runs sql scripts to create db structure, also there are scripts fill tables with test content.
4.  gradle build. Builds application and fetches dependencies.
5.  gradle tomcatRun. Runs embedded tomcat7. It will be displayed Building 75% - It's OK.
        OR copy 'userbook-1.0-SNAPSHOT.war' from build/libs to your servlet container(e.g. Tomcat) and rename war file    to 'its-userbook.war' and run server.
6.  Type http://localhost:8080/its-userbook/#/ in your browser.